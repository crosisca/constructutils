﻿// ECMAScript 5 strict mode
"use strict";

assert2(cr, "cr namespace not created");
assert2(cr.plugins_, "cr.plugins_ not created");

/////////////////////////////////////
// Plugin class
cr.plugins_.BM_api_construct2 = function(runtime)
{
    this.runtime = runtime;
};

(function ()
{
    var pluginProto = cr.plugins_.BM_api_construct2.prototype;
        
    /////////////////////////////////////
    // Object type class
    pluginProto.Type = function(plugin)
    {
        this.plugin = plugin;
        this.runtime = plugin.runtime;
    };

    var typeProto = pluginProto.Type.prototype;

    typeProto.onCreate = function()
    {
    };

    /////////////////////////////////////
    // Instance class
    pluginProto.Instance = function(type)
    {
        this.type = type;
        this.runtime = type.runtime;
    };

    var instanceProto = pluginProto.Instance.prototype;

    instanceProto.onCreate = function()
    {
        var self = this;
        
        window.addEventListener("resize", function () {
            self.runtime.trigger(cr.plugins_.BM_api_construct2.prototype.cnds.OnResize, self);
        });
        
        // register for update ready event and progress events
        if (typeof window.applicationCache !== "undefined")
        {
            window.applicationCache.addEventListener('updateready', function() {
                self.runtime.loadingprogress = 1;
                self.runtime.trigger(cr.plugins_.BM_api_construct2.prototype.cnds.OnUpdateReady, self);
            });
            
            window.applicationCache.addEventListener('progress', function(e) {
                self.runtime.loadingprogress = e["loaded"] / e["total"];
            });
        }
        
        // BM_api_construct2 visibility change events as well as platform-specific events like phonegap's
        // pause and resume will suspend the runtime.  handle this event as the 'page visible' trigger
        this.runtime.addSuspendCallback(function(s) {
            if (s)
            {
                self.runtime.trigger(cr.plugins_.BM_api_construct2.prototype.cnds.OnPageHidden, self);
            }
            else
            {
                self.runtime.trigger(cr.plugins_.BM_api_construct2.prototype.cnds.OnPageVisible, self);
            }
        });
        
        this.is_arcade = (typeof window["is_scirra_arcade"] !== "undefined");
    };
    
    /**BEGIN-PREVIEWONLY**/
    instanceProto.getDebuggerValues = function (propsections)
    {
        propsections.push({
            "title": "BM_api_construct2",
            "properties": [
                {"name": "User agent string", "value": navigator.userAgent, "readonly": true},
                {"name": "Document title", "value": document.title, "readonly": true},
                {"name": "Referrer", "value": document.referrer, "readonly": true},
                {"name": "Is online", "value": !!navigator.onLine, "readonly": true},
                {"name": "Is fullscreen", "value": !!(document["mozFullScreen"] || document["webkitIsFullScreen"] || document["fullScreen"] || this.runtime.isNodeFullscreen), "readonly": true},
                {"name": "Current URL", "value": window.location.toString(), "readonly": true},
                {"name": "Protocol", "value": window.location.protocol, "readonly": true},
                {"name": "Domain", "value": window.location.hostname, "readonly": true},
                {"name": "Path name", "value": window.location.pathname, "readonly": true},
                {"name": "Hash", "value": window.location.hash, "readonly": true},
                {"name": "Query string", "value": window.location.search, "readonly": true}
            ]
        });
    };
    /**END-PREVIEWONLY**/
    
    //////////////////////////////////////
    // Conditions
    function Cnds() {};
    
    Cnds.prototype.OnUpdateReady = function ()
    {
        return true;
    };
    
    Cnds.prototype.PageVisible = function ()
    {
        return !this.runtime.isSuspended;
    };
    
    Cnds.prototype.OnPageVisible = function ()
    {
        return true;
    };
    
    Cnds.prototype.OnPageHidden = function ()
    {
        return true;
    };
    
    Cnds.prototype.OnResize = function ()
    {
        return true;
    };
    
    pluginProto.cnds = new Cnds();

    //////////////////////////////////////
    // Actions
    function Acts() {};

    Acts.prototype.MoreGames = function ()
    {
        try {
            moregames.redirect();
        }
        catch(e) {
            console.log("More games");
        }
    };

    Acts.prototype.MainMenu = function ()
    {
        try {
            analytics.menu();
        }
        catch(e) {
            console.log("Main Menu");
        }
    };

    Acts.prototype.LevelStart = function ()
    {
        try {
            
        }
        catch(e) {
            console.log("Level Start");
        }
    };

    Acts.prototype.LevelFailed = function ()
    {
        try {
            analytics.LevelFailed(1);
            adSense.showAdvertising();
        }
        catch(e) {
            console.log("Level Failed");
        }
    };
    // If the games has multiple levels
    Acts.prototype.LevelComplete = function ()
    {
        try {
           analytics.level(1);
           adSense.showAdvertising();
        }
        catch(e) {
            console.log("Level Complete");
        }
    };
    // If the game is a 'one level only' game
    Acts.prototype.GameComplete = function ()
    {
        try {
            adSense.showAdvertising();
        }
        catch(e) {
            console.log("Game Complete");
        }
    };

    Acts.prototype.HighScore = function (score)
    {
        try {
            community.submitScore({score: gainedScore, callback: function () {
                adSense.showAdvertising();
                analytics.score(score);
            }});
        }
        catch(e) {
            console.log("Highscore" + score);
        }
    };

    Acts.prototype.RestartGame = function ()
    {
        try {
            
        }
        catch(e) {
            console.log("Restart Game");
        }
    };

    pluginProto.acts = new Acts();
    
    //////////////////////////////////////
    // Expressions
    function Exps() {};

    
    pluginProto.exps = new Exps();
    
}());