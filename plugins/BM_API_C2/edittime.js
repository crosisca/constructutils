﻿function GetPluginSettings()
        {
        return {
        "name":			"BM_api_construct2", // as appears in 'insert object' dialog, can be changed as long as "id" stays the same
                "id":		"BM_api_construct2", // this is used to identify this plugin and is saved to the project; never change it
                "version":	"3.0", // (float in x.y format) Plugin version - C2 shows compatibility warnings based on this
                "description":	"The Boostermedia API",
                "author":	"Boostermedia's Post-Production",
                "help url":	"www.boostermedia.com",
                "category":	"General", // Prefer to re-use existing categories, but you can set anything here
                "type":		"object", // either "world" (appears in layout and is drawn), else "object"
                "rotatable":	false, // only used when "type" is "world".  Enables an angle property on the object.
                "flags":        pf_singleglobal						// uncomment lines to enable flags...
                };
        };


//////////////////////////////////////////////////////////////
// Conditions

//////////////////////////////////////////////////////////////
// Actions
AddAction(0, 0, "MoreGames", "Main Menu", "MoreGames", "This event should be called when your More Games button is clicked", "MoreGames");
AddAction(1, 0, "MainMenu", "Main Menu", "MainMenu", "This event should be called when your Main menu is shown", "MainMenu");

AddAction(2, 0, "LevelStart", "In-game", "LevelStart", "This event should be called when a level is started", "LevelStart");
AddAction(3, 0, "LevelFailed", "In-game", "LevelFailed", "This event should be called when a level is failed", "LevelFailed");
AddAction(4, 0, "LevelComplete", "In-game", "LevelComplete", "This event should be called when a level is completed", "LevelComplete");
AddAction(5, 0, "GameComplete", "In-game", "GameComplete", "This event should be called when the game is completed (when it has more levels)", "GameComplete");

AddAnyTypeParam("Message", "Enter the message to display in the alert.", "Enter your Score variable without quotes!");
AddAction(6,0, "HighScore", "In-game", "HighScore", "This event should be called when a player loses all his lives or when the game is completed", "HighScore");
AddAction(7,0, "Restart", "In-game", "Restart", "This event should be called when a player wants to restart the game when the game is won or lost", "RestartGame")

ACESDone();

// Property grid properties for this plugin
var property_list = [
        ];
        
// Called by IDE when a new object type is to be created
function CreateIDEObjectType()
{
        return new IDEObjectType();
}

// Class representing an object type in the IDE
function IDEObjectType()
{
        assert2(this instanceof arguments.callee, "Constructor called as a function");
}

// Called by IDE when a new object instance of this type is to be created
IDEObjectType.prototype.CreateInstance = function(instance)
{
        return new IDEInstance(instance, this);
}

// Class representing an individual instance of an object in the IDE
function IDEInstance(instance, type)
{
        assert2(this instanceof arguments.callee, "Constructor called as a function");
        
        // Save the constructor parameters
        this.instance = instance;
        this.type = type;
        
        // Set the default property values from the property table
        this.properties = {};
        
        for (var i = 0; i < property_list.length; i++)
                this.properties[property_list[i].name] = property_list[i].initial_value;
}

// Called by the IDE after all initialization on this instance has been completed
IDEInstance.prototype.OnCreate = function()
{
}

// Called by the IDE after a property has been changed
IDEInstance.prototype.OnPropertyChanged = function(property_name)
{
}
        
// Called by the IDE to draw this instance in the editor
IDEInstance.prototype.Draw = function(renderer)
{
}

// Called by the IDE when the renderer has been released (ie. editor closed)
// All handles to renderer-created resources (fonts, textures etc) must be dropped.
// Don't worry about releasing them - the renderer will free them - just null out references.
IDEInstance.prototype.OnRendererReleased = function()
{
}
